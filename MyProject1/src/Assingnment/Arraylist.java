package Assingnment;

import java.util.ArrayList;
import java.util.Iterator;



public class Arraylist {



   public static void main(String[] args) {
        
        ArrayList<Integer>arrylist= new ArrayList<Integer>();
        arrylist.add(14);
        arrylist.add(3);
        arrylist.add(99);
        
        System.out.println("using the for loop");
        for(int i=0;i<arrylist.size();i++)
        {
            System.out.println(arrylist.get(i));
        }
            
        System.out.println("using while loop");
        int  i=0;
        while(arrylist.size()>i)
        {
            System.out.println(arrylist.get(i));
            i++;
        }
        
        System.out.println("using iterator");
          Iterator<Integer> Array = arrylist.iterator();
        while(Array.hasNext())
        {
            System.out.println(Array.next());
        }
        
        System.out.println("using advance loop");
        for (Integer number:arrylist)
        {
            System.out.println(number);
        }


    }
}


