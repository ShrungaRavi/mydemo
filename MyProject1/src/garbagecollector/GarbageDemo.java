package garbagecollector;

public class GarbageDemo {
	public void finalize()  {
		System.out.println("unreferenced object garbage collected");
	
	}

	public static void main(String[] args) {
		GarbageDemo gd=new GarbageDemo();
		gd=null;
		GarbageDemo gd2=new GarbageDemo();
		gd=gd2;
		new GarbageDemo();
		System.gc();
		
		// TODO Auto-generated method stub

	}

}
