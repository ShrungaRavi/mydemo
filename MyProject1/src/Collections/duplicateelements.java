package Collections;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class duplicateelements 
{

	public static void main(String[] args)
	{
		Integer arr[] = {90,20,20,30,40,10,10,30,60,80};
		System.out.println("Original array:" + Arrays.toString(arr));
		
		removeDuplicates(arr);
	}
	public static void removeDuplicates(Integer[] arr)
	{
		List<Integer>list =Arrays.asList(arr);
		List<Integer>ListWithoutDupes= list.stream().distinct().collect(Collectors.toList());
		System.out.println("After Removing the Duplicates:"+ListWithoutDupes);
	}
}

