package Collections;

import java.util.Scanner;

public class BigNum {

		// TODO Auto-generated method stub
		public static void main(String[] args) 
		{
			Scanner scan= new Scanner(System.in);
			System.out.println("Enter the number of elements in an array");
			int min;
			int n= scan.nextInt();
			int[] arr=new int[n];
			for(int i=0;i<n;i++)
				{
				  System.out.print("Enter the element "+(i+1)+": ");
				  arr[i]=scan.nextInt();
				}
			min=arr[0];
			for(int i=0;i<n;i++)
			{
				if(min<arr[i])
				{
					min=arr[i];
				}
			}
			System.out.println("\nThe Largest Elements is:"+min);
			
		}

	}

