package polymorphism;


class Vehicle2 {


				//defining a method
				void run() 
				{
					System.out.println("vehicle is running");
				}
}
			
			class Vehicle extends Vehicle2
			{
				// defining the same method as in the parent class
				void run() 
				{
					System.out.println("bike is running safely");
				}
			
public static void main(String[] args)
				{
					
					Vehicle obj=new Vehicle();
					obj.run();// calling method
				}

		

	}


